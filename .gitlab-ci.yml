
# NOTE: keep in mind this file is _included_ by _other_ repos, and thus the env var names
# are not _always_ related to _this_ repo ;-)

# A GitLab group (ideally) or project will need to set [Settings] [CI/CD] [Variables]
#   NOMAD_ADDR
#   NOMAD_TOKEN
# to whatever your Nomad cluster was setup to.


# NOTE: very first pipeline, the [build] below will make sure this is created
image: registry.gitlab.com/internetarchive/nomad/master:latest


stages:
  - build
  - test
  - deploy
  - cleanup


include:
  # GitLab Auto DevOps' stock CI/CD [build] phase:
  - remote: 'https://gitlab.com/gitlab-org/gitlab-foss/-/raw/master/lib/gitlab/ci/templates/Jobs/Build.gitlab-ci.yml'
  # @see https://gitlab.com/gitlab-org/cluster-integration/auto-build-image/blob/master/src/build.sh


.nomad-vars:
  before_script:
    # make nice hostname, eg:
    #   services-timemachine.x.archive.org
    #   ia-petabox-webdev-3939-fix-things.x.archive.org
    # and a unique slug that factors in group, project, and branch (when not master)
    - |
      BRANCH_PART=""
      [ "$CI_COMMIT_REF_SLUG" != "master" ]  &&  BRANCH_PART="-${CI_COMMIT_REF_SLUG}"
      export NOMAD__SLUG=$(echo "${CI_PROJECT_PATH_SLUG}${BRANCH_PART}" |cut -b1-63)

      DOM=${KUBE_INGRESS_BASE_DOMAIN}
      [ "$NOMAD__HOSTNAME" = "" ]  &&  export NOMAD__HOSTNAME="${NOMAD__SLUG}.${DOM}"


deploy:
  extends: .nomad-vars
  stage: deploy
  script:
    # You can have your own/custom `project.nomad` in the top of your repo - or we'll just use
    # this fully parameterized nice generic 'house style' project
    - if [ ! -e project.nomad ];then wget -q https://gitlab.com/internetarchive/nomad/-/raw/master/project.nomad; fi

    # Swap out any env vars in `project.nomad` with environment values
    - node -e 'console.log(JSON.stringify(process.env))' >| /tmp/env.json
    - levant render -var-file=/tmp/env.json  project.nomad 2>/dev/null >| project.hcl
    - rm /tmp/env.json

    - echo deploying to https://$NOMAD__HOSTNAME
    - nomad validate project.hcl
    - nomad plan     project.hcl 2>&1 |sed 's/\(password[^ \t]*[ \t]*\).*/\1 ... /' || echo
    - nomad run      project.hcl
    - echo deployed to https://$NOMAD__HOSTNAME

  environment:
    name: $CI_COMMIT_REF_SLUG
    url: https://$NOMAD__HOSTNAME
    on_stop: stop_review
  rules:
    - if: '$NOMAD__NO_DEPLOY'
      when: never
    - when: always


stop_review:
  # See:
  # https://gitlab.com/gitlab-org/gitlab-foss/blob/master/lib/gitlab/ci/templates/Jobs/Deploy.gitlab-ci.yml
  extends: .nomad-vars
  stage: cleanup
  variables:
    GIT_STRATEGY: none
  script:
    - nomad stop $NOMAD__SLUG
  environment:
    name: $CI_COMMIT_REF_SLUG
    action: stop
  dependencies: []
  allow_failure: true
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: never
    - if: '$NOMAD__NO_DEPLOY'
      when: never
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
      when: manual
